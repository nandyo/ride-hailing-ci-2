import { startServer } from "./server";
import { syncDB } from "./orm";
import { connectToBus } from "../lib/bus";
import { positionProjector } from "./position";

async function startApp() {
  await syncDB();
  await connectToBus();
  positionProjector();
  startServer();
}

//release this when things get out of hand (connection_err_postgre)
/* setTimeout( */startApp()/* , 10000) */;
