import { startServer } from './server';
import { syncDB } from './orm';
import { connectToBus } from '../lib/bus';

async function startApp() {
  await syncDB();
  await connectToBus();
  startServer();
}

//release this when things get out of hand (connection_err_postgre)
/* setTimeout( */startApp()/* , 10000) */;
