import { random } from "faker";
import { expect } from "chai";
import { get, post } from "request-promise-native";
import { dbQuery, syncDB } from "./db";
import { bus } from "./bus";

const TRACKER_HOST = process.env["TRACKER_HOST"] || "localhost";
const TRACKER_PORT = process.env["TRACKER_PORT"] || 3000;

describe("Tracker Server", function(){
    //release this when things get out of hand (connection_err_postgre)
    this.timeout(30000);
    before(function(done){
        setTimeout(done, 15000);
    });

    beforeEach(async function(){
        await syncDB();
        
        //reset database
        await dbQuery({ type: "remove", table: "track_events" });

        //feed data
        this.tracks = (await dbQuery({
            type: "insert",
            table: "track_events",
            values: {
                "rider_id": 4,
                "north": random.number({ min: 1, max: 20 }),
                "south": random.number({ min: 1, max: 20 }),
                "west": random.number({ min: 1, max: 20 }),
                "east": random.number({ min: 1, max: 20 }),
                "createdAt": new Date(),
                "updatedAt": new Date()
            },
            returning: [
                "rider_id",
                "north",
                "south",
                "west",
                "east",
                "createdAt"
            ]
        })) [0][0];
        //
    });
    describe("Movement", function(){
        it("harusnya memberikan data suatu rider", async function(){
            const response = await get(`http://${TRACKER_HOST}:${TRACKER_PORT}/movement/4`, {json: true});
            expect(response.ok).to.be.true;
            console.log(response.logs[0]);
            console.log(this.tracks);
            expect(response.logs[0].north).to.be.eq(this.tracks.north);
            expect(response.logs[0].east).to.be.eq(this.tracks.east);
            expect(response.logs[0].south).to.be.eq(this.tracks.south);
            expect(response.logs[0].west).to.be.eq(this.tracks.west);
            expect(response.logs[0].time).to.be.eq(this.tracks.createdAt.toISOString());
        });
    });
    // describe("Tracker", function(){
    //     it("harusnya mempublish event rider.moved", async function(){
    //         const response = await post(`http://${TRACKER_HOST}:${TRACKER_PORT}/track`, {
    //             body:{
    //                 north: 1,
    //                 south: 0,
    //                 west: 0,
    //                 east: 0,
    //                 rider_id: 1
    //             },
    //             json: true
    //         });

    //         console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+response);
            
    //         bus.subscribe("rider.moved", (responsebody) => {
    //             console.log("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"+responsebody);
    //         });
    //     });
    // });
});